import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, View , Alert} from 'react-native';
import Button from './component/Button';
import Gap from './component/Gap';

export default class Login extends Component{
	
	constructor(props) {
		//constructor to set default state
		super(props);
		this.state = {
			username: '',
			password: '',
			isError: false,
		};
	  }

	  load() {
		  if(this.state.username == "" && this.state.password == ""){
			Alert.alert(
                "Warning",
                "Field Tidak Boleh Kosong",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
		  }else if(this.state.username == "" || this.state.password == ""){
			Alert.alert(
                "Warning",
                "Field Tidak Boleh Kosong",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
		  }
		  else {
			this.props.navigation.navigate('Home' ,  {username: this.state.username})
		  }
	  }
	render(){
		const { navigate } = this.props.navigation;
		return (
			<View style={style.views}>
				<View style={style.content}>
					<Text style={style.text}>Sign In</Text>
					<Text style={style.pls}>Please Fill Your Information</Text>
					<Text>Email</Text>
					<TextInput 
						style={{borderWidth : 1 , borderRadius : 8 , marginTop : 10}}
						value={this.state.username}  
						onChangeText={username => this.setState({ username })}
          				placeholder={'Enter Email'} />
					<Gap height={25} />
					<Text>Password</Text>
					<TextInput 
					secureTextEntry={true}
						style={{borderWidth : 1 , borderRadius : 8 , marginTop : 10}}
						value={this.state.password}  
						onChangeText={password => this.setState({ password })}
          				placeholder={'Enter Password'} />
					<Gap height={40} />
					<Button title="Sign In" type="SignIn" onPress={() => this.load()} />
					<Gap height={25} />
					
				</View>
			</View>
		);
		}

}


const style = StyleSheet.create({
	views: {
		flex: 1,
		paddingTop: 25,
	},

	content: {
		paddingTop: 70,
		padding: 40,
	},
	text: {
		marginBottom: 8,
		fontSize: 25,
		fontWeight: 'bold',
	},
	pls: {
		marginBottom: 25,
		fontSize: 14,
		fontWeight: 'normal',
		color: '#989595',
	},
	email: {
		fontSize: 14,
		fontWeight: 'normal',
		color: '#989595',
	},
});
