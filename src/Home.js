import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, Image, ActivityIndicator , ImageBackground , ScrollView , TouchableOpacity} from 'react-native';
import Axios from 'axios';
import Gap from './component/Gap';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false
    };
  }

  // Mount User Method
  componentDidMount() {
    this.getGithubUser()
  }

  //   Get Api Users
  getGithubUser = async () => {
    try {
      const response = await Axios.get(`https://api.themoviedb.org/3/discover/movie?api_key=b3d25a13cfc040b3812a0afbc7ff5c32&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=3`)
      this.setState({ isError: false, isLoading: false, data: response.data })
      console.log("wertyui" + response)
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }
  
  render() {
    const { navigate } = this.props.navigation;
    //  If load data
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    // If data not fetch
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
    // If data finish load
    return (
      <View style={styles.container}>
			<TouchableOpacity style={styles.header} onPress={() => this.props.navigation.navigate('About')}>
				<View style={styles.flex}>
    <Text style={styles.name}>{this.props.route.params.username}</Text>
					<Text style={styles.desc}>I'm Mobile Developers</Text>
				</View>
				<Image source={require('./images/Image.png')} style={styles.img} />
			</TouchableOpacity>
        <Gap height={20}/>
        <View style={{padding : 20}}>
          <Text style={styles.textcategory}>Top Category</Text>
          <Gap height={28}/>
          <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{justifyContent: 'space-between'}}>
            <FlatList 
                  data={this.state.data.results}
                  horizontal={true}
                  renderItem={({ item }) =>
                    <TouchableOpacity onPress={() => 
                  navigate('Detail' ,{
                      IDNYA : item.id ,
                      judulnya : item.title,
                      img : item.poster_path,
                      overview : item.overview,
                      release_date : item.release_date,
                    })}>
                      <Image source={{ uri: "https://image.tmdb.org/t/p/w500/"+`${item.poster_path}` }} style={styles.Image}/>
                    
                    </TouchableOpacity>
                  }
              keyExtractor={({ id }, index) => index}
            />
          </View>
          <Gap height={20}/>
          <Text style={styles.textcategory}>Newest</Text>
          <Gap height={28}/>
          <View>
            <FlatList 
                  data={this.state.data.results}
                  renderItem={({ item }) =>
                    <TouchableOpacity  key={item.id} onPress={() => 
                  navigate('Detail' ,{
                      IDNYA : item.id ,
                      judulnya : item.title,
                      img : item.poster_path,
                      overview : item.overview,
                      release_date : item.release_date,
                    })}>
                      <View style={styles.viewList}>
                          <View>
                            <Image source={{ uri: "https://image.tmdb.org/t/p/w500/"+`${item.poster_path}` }} style={styles.img2} />
                          </View>
                          <View>
                          <Text style={styles.textItemLogin}> {item.title}</Text>
                        <Text style={styles.textItemUrl}> {item.release_date}</Text>
                      
                          </View>
                       </View>
                    </TouchableOpacity>
                  }
              keyExtractor={({ id }, index) => index}
              
            />
          </View>
        
      </ScrollView>
      </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  textcategory: {
    fontSize : 16,
  },
  container: { flex: 1},
  flex: { flex: 1 },
  header: {
		backgroundColor: '#2C1F62',
		height: 112,
		borderRadius: 25,
		borderTopRightRadius: 0,
		borderTopLeftRadius: 0,
		flexDirection: 'row',
	},
	name: {
		marginTop: 27,
		fontSize: 18,
		fontWeight: 'bold',
    marginLeft: 25,
    color:'white'
	},
	desc: {
		marginTop: 8.09,
		marginLeft: 25,
    color: '#4D4545',
    color:'white'
	},
  img2: {
    width: 88,
    height: 80,
    borderRadius: 40
  },
  viewList: {
    height: 100,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#DDD',
    alignItems: 'center',
  },
  Image: {
    width: 90,
    height: 140,
    borderRadius : 15,
    paddingLeft : 15
    
  },
  textItemLogin: {
    fontWeight: 'bold',
    textTransform: 'capitalize',
    marginLeft: 20,
    fontSize: 16,
  },
  textItemUrl: {
    fontWeight: 'bold',
    marginLeft: 20,
    fontSize: 12,
    marginTop: 10,
    color: 'blue'
  },
  img: { width: 82, height: 66, marginRight: 25, marginTop: 16 },
})