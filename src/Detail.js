// Second screen which we will use to get the data
import React, { Component } from 'react';
//import react in our code.
import { StyleSheet, View, Text , FlatList, Image} from 'react-native';
//import all the components we are going to use.
import Gap from './component/Gap'
export default class Detail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false
    };
  }

  // Mount User Method
  componentDidMount() {
    this.getGithubUser()
  }

  //   Get Api Users
  getGithubUser = async () => {
    try {
      const response = await Axios.get(`https://api.themoviedb.org/3/movie/531499?api_key=b3d25a13cfc040b3812a0afbc7ff5c32&language=en-US`)
      this.setState({ isError: false, isLoading: false, data: response.data })
      console.log(response)
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }
 

  render() {
    //  If load data
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    // If data not fetch
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
}
  render() {
    // const { navigate } = this.props.navigation;
   const uwuwu = this.props.route.params.IDNYA;
   const qqqq = this.state.data.production_companies;
    console.log(qqqq)
    return (
      //View to hold our multiple components
      <View style={styles.container}>
        <Image source={{uri : 'https://image.tmdb.org/t/p/w500/'+this.props.route.params.img}} style={styles.img}/>
        <Gap height={39}/>
        <View style={{paddingLeft:20}}>
          <Text style={styles.TextStyle}>{this.props.route.params.judulnya}</Text>
          <Gap height={8}/>
          <Text style={styles.release}>{this.props.route.params.release_date}</Text>
          <Gap height={25}/>
          <Text>StoryLine</Text>
          <Gap height={10}/>
          <Text style={styles.overview}>{this.props.route.params.overview}</Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  release:{
    fontSize : 12
  },
  img: {
    width : '100%',
    height : '45%'
  },
  TextStyle: {
    fontSize: 22,
    color: '#2C1F62',
    fontWeight : 'bold'
  },
  overview: {
    fontSize: 16,
    color: 'black',
    paddingRight : 20
  },
});