import React from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import Gap from './component/Gap';
import Button from './component/Button';

const App = ({ navigation }) => {
	return (
		<ScrollView>
			<View style={style.views}>
				<View style={style.content}>
					<View style={style.img}>
						<Image
							source={require('./images/Ellipse3.png')}
							style={{ width: 150, height: 150, marginBottom: 25 }}
						/>
					</View>
					<Gap height={40} />
					<Text style={style.text}>Ryan Hartadi</Text>
					<Text style={style.pls}>I'm Mobile Developers</Text>
					<Gap height={40} />
					<Text style={style.acc}>My Social Media</Text>
					<View
						style={{
							flexDirection: 'row',
							borderRadius: 7,
							borderWidth: 1,
							borderColor: '#E5E5E5',
							marginBottom: 25,
						}}
					>
						<Image
							source={require('./images/ig.png')}
							style={{ width: 71, height: 60, marginBottom: 25 }}
						/>
						<View>
							<Gap height={8} />
							<Text style={{ marginLeft: 40, fontSize: 18 }}>Instagram Account</Text>
							<Gap height={8} />
							<Text style={{ marginLeft: 40 }}>@ryanhartadi999</Text>
						</View>
					</View>
					<View
						style={{
							flexDirection: 'row',
							borderRadius: 7,
							borderWidth: 1,
							borderColor: '#E5E5E5',
							marginBottom: 25,
						}}
					>
						<Image
							source={require('./images/fb.png')}
							style={{ width: 71, height: 60, marginBottom: 25 }}
						/>
						<View>
							<Gap height={8} />
							<Text style={{ marginLeft: 40, fontSize: 18 }}>Facebook Account</Text>
							<Gap height={8} />
							<Text style={{ marginLeft: 40 }}>Ryan Hartadi</Text>
						</View>
					</View>
					<View
						style={{
							flexDirection: 'row',
							borderRadius: 7,
							borderWidth: 1,
							borderColor: '#E5E5E5',
							marginBottom: 25,
						}}
					>
						<Image
							source={require('./images/git.png')}
							style={{ width: 71, height: 60, marginBottom: 25 }}
						/>
						<View>
							<Gap height={8} />
							<Text style={{ marginLeft: 40, fontSize: 18 }}>Github Account</Text>
							<Gap height={8} />
							<Text style={{ marginLeft: 40 }}>https://github.com/RyanHartadi06</Text>
						</View>
					</View>

					<Button type="SignIn" title="Log Out"/>
				</View>
			</View>
		</ScrollView>
	);
};

export default App;

const style = StyleSheet.create({
	views: {
		flex: 1,
		paddingTop: 25,
	},
	img: {
		alignItems: 'center',
	},
	content: {
		paddingTop: 70,
		padding: 40,
	},
	text: {
		marginBottom: 8,
		fontSize: 25,
		fontWeight: 'bold',
		textAlign: 'center',
	},
	pls: {
		marginBottom: 25,
		fontSize: 14,
		fontWeight: 'normal',
		color: '#989595',
		textAlign: 'center',
	},
	acc: {
		marginBottom: 25,
		fontSize: 14,
		fontWeight: 'normal',
		color: '#989595',
	},
});
